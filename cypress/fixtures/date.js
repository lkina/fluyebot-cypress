const today = new Date();

export const date = 
String(today.getFullYear()) +
String(('0' + (today.getMonth()+1)).slice(-2)) +
String(('0' + today.getDate()).slice(-2))