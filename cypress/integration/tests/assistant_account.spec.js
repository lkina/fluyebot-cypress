import {superadmin} from "../../fixtures/usersData"
import {date} from "../../fixtures/date"

context('Assistant or Bot accounts', () => {

    before(() => {
        cy.clearCookies()
        cy.intercept('GET', '**/user/auth').as('getAuth')
        cy.intercept('GET', '**/panel/conversations/accounts').as('getAccounts')
        cy.intercept('GET', '**/panel/conversations*').as('getConversations')
        cy.visit('/')
        cy.login(superadmin)
        cy.wait(['@getAuth', '@getConversations', '@getAccounts'])
    })

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('fluyebot_session')
        Cypress.Cookies.preserveOnce('XSRF-TOKEN')
    })

    describe('Paths and Alerts', () => {

        it('Go to Cuentas', () => {
            cy.get('a.nav-link')
                .contains('Cuentas')
                .click()
            cy.url().should('include', '/panel/cuentas')
        })
    
        it('Go to Quantico Bots', () => {
            //On Cuentas
            cy.visit('/panel/cuentas')
            cy.get('#tbl-accounts tbody tr').eq('-2')
                .find('td:first').should('have.text', 'Quantico')
            //Clic on options
            cy.get('#tbl-accounts tbody tr').eq('-2')
                .find('.btn-group > .btn')
                .click()
            cy.get('.dropdown-menu')
                .should('be.visible')
            //Clic on Lista de Asistentes
            cy.get('.btn-group .dropdown-menu.show a').eq('3')
                .should('contain.text', 'Lista de asistentes' )
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.url().should('include', 'panel/asistentes')
            cy.get('.header h2').should('contain.text', 'Quantico - Lista de asistentes')
        })
        
        it('Go to New Bot', () => {
            //On Lista de Asistentes in Quantico account options
            cy.visit('/panel/asistentes/3')
            //Go to Crear nuevo asistente
            cy.get('.header > .btn')
                .click()
            cy.url().should('include', '/asistente/crear/3')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('h2')
                .should('contain.text', 'Quantico - Asistente')
        })

        it('Return to Bot List', () => {
            cy.visit('/panel/asistente/crear/3')
            cy.get('#assistant_form .btn-outline-secondary').contains('Lista de asistentes')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.url().should('include', '/panel/asistentes/3')
            cy.visit('/panel/asistente/crear/3')
            cy.get('.breadcrumb-item').contains('Ver lista de asistentes')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.url().should('include', '/panel/asistentes/3')
        })

        it('Return to Accounts', () => {
            cy.visit('/panel/asistentes/3')
            cy.get('.breadcrumb-item').contains('Ver todas las cuentas')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.url().should('include', '/panel/cuentas')
        })
    
        it('Check alert messages in New Bot Form',() => {
            //On new assistant for Quantico account
            cy.visit('/panel/asistente/crear/3')
            //Empty email alert
            cy.get('#name')
                .type('Asistente de prueba')
            cy.get('#assistant_form').find('[type="submit"]')
                .should('contain.text', 'Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.help-block')
                .should('contain.text', 'El campo correo electrónico es obligatorio.')
            //Invalid email alert
            cy.get('#email')
                .type('email inválido{enter}')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.help-block')
                .should('contain.text', 'El campo correo electrónico debe ser una dirección de correo válida.')
            //Duplicated email alert
            cy.get('#email').clear()
            .type('editemail@quanticotrends.com{enter}')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.help-block')
            .should('contain.text', 'El valor del campo correo electrónico ya está en uso.')
        })
    })

    describe('Modify Bots', () => {

        const inputData = 'Asistente de prueba ' + date

        it('Create a New Bot', () => {
            cy.visit('/panel/asistente/crear/3')
            //Enter name and signature
            cy.get('#name')
                .type(inputData)
            cy.get('.note-editable.card-block')
                .type('Asistente de prueba{enter}ATENCIÓN AL CLIENTE{enter}FLUYE - www.fluye.com{enter}+511 739 0777 Anexo 108{selectall}')
            cy.get('[aria-label="Bold (CTRL+B)"]').click()
            //Type bot email
            cy.get('#email')
                .type('bot' + date + '@quanticotrends.com')
            cy.get('#assistant_form').find('[type="submit"]')
                .should('contain.text', 'Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.alert')
                .should('contain.text', '¡Asistente grabado exitosamente!')
        })
    
        it('Edit a Bot', () => {
            cy.visit('/panel/asistentes/3')
            cy.get('#asistentes tbody tr:first .btn-secondary').click()
            //Enter name and signature
            cy.get('#name').clear()
                .type('Asistente de prueba')
            cy.get('.note-editable.card-block').clear()
                .type('Asistente de prueba{enter}ATENCIÓN AL CLIENTE{enter}FLUYE - www.fluye.com{enter}+511 739 0777 Anexo 108')
            cy.get('#assistant_form').find('[type="submit"]')
                .should('contain.text', 'Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.alert')
                .should('contain.text', '¡Asistente grabado exitosamente!')
    
        })
    
        it('Delete a Bot', () => {
            cy.visit('/panel/asistentes/3')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('#asistentes tbody tr:last td:first')
                .should('contain.text', inputData)
            cy.get('#asistentes tbody tr:last .btn-danger')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('#btnYes').contains('Borrar')
                .click()
            cy.get('#asistentes tbody tr')
                .should('have.length', '1')
        })
    })
})
