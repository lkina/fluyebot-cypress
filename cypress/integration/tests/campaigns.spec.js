import {admin} from "../../fixtures/usersData"
import {date} from "../../fixtures/date"

context('Campaigns', () => {
    before(() => {
        cy.clearCookies()
        cy.intercept('GET', '**/user/auth').as('getAuth')
        cy.intercept('GET', '**/panel/campanias*').as('getCampanias')
        cy.intercept('GET', '**/panel/conversations*').as('getConversations')
        cy.visit('/')
        cy.login(admin)
        cy.wait(['@getAuth', '@getCampanias', '@getConversations'])
    })

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('fluyebot_session')
        Cypress.Cookies.preserveOnce('XSRF-TOKEN')
    })

    describe('Paths and Alerts', () => {

        it('Go to New Campaign', () => {
            //Go to Campañas
            cy.get('a.nav-link')
                .contains('Campañas')
                .click()
            cy.wait(1000)
            cy.url().should('include', '/panel/campanias')
            //Go to Nueva Campaña
            cy.contains('Crea nueva campaña')
                .click()
            cy.url().should('include', '/panel/campania/crear')
        })

        it('Go back to Campaigns', () => {
            cy.get('.breadcrumb a').click()
            cy.url()
                .should('include', '/panel/campanias')
        })

        it('Check alert messages on New Campaign Form', () => {
            cy.visit('/panel/campania/crear')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Enter name
            cy.get('#name')
                .type('Campaña de prueba')
            //Enter description
            cy.get('#description')
                .type('Esta es una campaña de prueba')
            //Enter subject
            cy.get('#subject_email')
                .clear()
                .type('Hola ')
            //Check selecting variables in subject
            cy.get('#value_var')
                .select('$_LEAD_NOMBRE')
            cy.get('#subject_email')
                .type(', esto es una prueba.')
            cy.contains('Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.alert')
                .should('contain.text', 'El campo addressees es obligatorio.')
                .and('contain.text', 'El campo executives es obligatorio.')
        })

        it('Check checkboxes and other options', () => {
            cy.visit('/panel/campania/crear')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Duración de la campaña     
            cy.get('#days_business')
                .clear()
                .type('23')
            //Checkbox campaña permanente
            cy.get('#is_permanent')
                .click({force:true})
            cy.get('#days_business')
                .should('have.attr', 'readonly')
            cy.get('#hdDaysBusiness')
                .should('have.value', '23')
        })
    })

    describe('Modify Campaigns', () => {

        const inputData = 'Campaña de prueba ' + date

        it('Create a New Campaign', () => {
            cy.visit('/panel/campania/crear')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Enter name
            cy.get('#name')
                .type(inputData)
            //Enter description
            cy.get('#description')
                .type('Esta es una campaña de prueba')
            //Select Tipo de campaña
            cy.get('#campaign_type_id')
                .select('Cobranza')
            //Uncheck Campaña de prueba
            cy.get('.content-fancy-checkbox > .fancy-checkbox > span')
                .click()
            //Select Lista de destinatarios
            cy.get('#addressees')
                .select('Test List ID 108')
            //Select Flujo de secuencias
            cy.get('#flow')
                .select('Flujo de prueba')
            //Select Asistentes
            cy.get('#assistants_ids.form-control')
                .select('Asistente de prueba')
            //Select ejectutivos
            cy.get('.fancy-checkbox')
                .contains('Ejecutivo Maestro')
                .click()
            //Enter subject
            cy.get('#subject_email')
                .type('Hola $_LEAD_NOMBRE, esto es una prueba.')
            //Save
            cy.contains('Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Assert on alert and status
            cy.get('.alert')
                .should('contain.text', '¡Campaña grabada exitosamente!')
            cy.get('#basic-form label')
                .should('contain.text', 'Estado: NO INICIADA')
            //Assert on checkbox Campaña de prueba after saving
            cy.get('.content-fancy-checkbox > .fancy-checkbox > input')
                .should('not.be.checked')
        })

        it('Edit a existing Campaign', () => {
            cy.visit('/panel/campanias')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Go to the last created campaing
            cy.get('#campanias tbody tr:first td:last a')
                .click()
            cy.url().should('include', '/panel/campania/editar')
            //Check campaign name
            cy.get('#name')
                .should('have.value', inputData)
            //Select Tipo de campaña
            cy.get('#campaign_type_id')
                .select('Ventas')
            //Check Campaña de prueba
            cy.get('.content-fancy-checkbox > .fancy-checkbox > span')
                .click()
            //Select Asistentes
            cy.get('#assistants_ids.form-control')
                .select('Asistente de prueba')
            //Save
            cy.contains('Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json            
            //Assert on alert and status
            cy.get('.alert')
                .should('contain.text', '¡Campaña grabada exitosamente!')
            cy.get('#basic-form label')
            //Assert on checkbox Campaña de prueba after saving
            cy.get('.content-fancy-checkbox > .fancy-checkbox > input')
                .should('to.be.checked')
        })

        it('Start a Campaign', () => {
            cy.visit('/panel/campanias')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Go to the last created campaing
            cy.get('#campanias tbody tr:first td:last .d-inline')
                .click()
            cy.get('#swal2-content')
                .should('contain.text', 'Estas a punto de enviar una campaña con 10 destinatarios.')
                .and('contain.text', inputData)
                .and('contain.text', 'Quantico')
                .and('contain.text', 'Asistente de prueba')
                .and('contain.text', 'Hola Lucia, esto es una prueba.')
            cy.get('#swal2-content .body')
                .should('have.length', 11)
            cy.get('button.swal2-confirm')
                .should('not.be.disabled')
            cy.get('.swal2-cancel')
                .click()
        })
    })
})

