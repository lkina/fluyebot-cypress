import {superadmin} from "../../fixtures/usersData"
import {date} from "../../fixtures/date"

context('Client accounts', () => {

    before(() => {
        cy.clearCookies()
        cy.intercept('GET', '**/user/auth').as('getAuth')
        cy.intercept('GET', '**/panel/conversations/accounts').as('getAccounts')
        cy.intercept('GET', '**/panel/conversations*').as('getConversations')
        cy.visit('/')
        cy.login(superadmin)
        cy.wait(['@getAuth', '@getConversations', '@getAccounts'])
    })

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('fluyebot_session')
        Cypress.Cookies.preserveOnce('XSRF-TOKEN')
    })

    describe('Paths and Alerts', () => {

        it('Go to New Client Account', () => {
            //Go to cuentas
            cy.get('a.nav-link')
                .contains('Cuentas')
                .click()
            cy.url().should('include', '/panel/cuentas')
            //Go to Crear cuenta
            cy.contains('Crear cuenta')
                .click()
            cy.url().should('include', '/panel/cuenta/crear')
        })

        it('Go to Edit a Client Account', () => {
            cy.visit('/panel/cuentas')
            cy.get('#tbl-accounts tbody tr:first td:first')
                .should('contain.text', 'A1')
            cy.get('#tbl-accounts tbody tr:first .btn-actions-bg')
                .click()
            cy.get('.dropdown-menu')
                .should('be.visible')
            cy.get('.dropdown-menu .dropdown-item:first')
                .click()
            cy.url()
                .should('include', 'panel/cuenta/editar/9')
        })
    
        it('Check Alert messages in New Account Form', () => {
            //On Crear Cuenta
            cy.visit('/panel/cuenta/crear')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Enter data
            cy.get('#name')
                .type('Empresa de prueba')
            cy.get('#given_name')
                .type('Cliente de prueba')
            cy.get('#fathers_last_name')
                .type('Apellidos')
            cy.get('#email')
                .type('cliente@empresa')
            cy.get('#password')
                .type('empresa2021')
            cy.get('#password-confirm')
                .type('empresa20')
            cy.contains('Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.help-block.text-danger')
                .should('contain.text', 'El campo correo electrónico debe ser una dirección de correo válida.')
                .and('contain.text', 'El campo teléfono debe contener entre 7 y 15 dígitos.')
                .and('contain.text', 'El campo confirmación de contraseña no coincide.')
        })

        it('Check the Checkbox', () => {
            //On Cuentas
            cy.visit('/panel/cuenta/crear')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.fancy-checkbox>span').click()
            cy.get('#contact_email').type('some@email.com')
        })
    })

    describe('Modify Client Acounts', () => {

        const inputData = 'Z Empresa de prueba ' + date

        it('Create a New Client Account', () => {
            
            cy.visit('/panel/cuenta/crear')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Enter data
            cy.get('#name')
                .type(inputData)
            cy.get('#given_name')
                .type('Cliente de prueba')
            cy.get('#fathers_last_name')
                .type('Apellidos')
            cy.get('#email')
                .type('cliente' + date + '@empresa.com')
            cy.get('#phone_number')
                .type('999999999')
            cy.get('#password')
                .type('empresa2021')
            cy.get('#password-confirm')
                .type('empresa2021')
            cy.contains('Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.alert')
                .should('contain.text', inputData + ' fue registrada')
            cy.url().should('include', 'panel/cuentas')
        })
    
        it('Edit a Client Account', () => {
            cy.visit('panel/cuenta/editar/9')
            //Enter name and signature
            cy.get('#name').clear()
                .type('A1')
            cy.get('#given_name').clear()
                .type('A1')
            cy.get('#fathers_last_name').clear()
                .type('Empresa')
            cy.get('#email').clear()
                .type('cuenta@empresaa1.com')
            cy.get('#phone_number').clear()
                .type('999999999')
            cy.get('#password').clear()
                .type('empresa2021')
            cy.get('#password-confirm').clear()
                .type('empresa2021')
            cy.contains('Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.alert')
                .should('contain.text', 'La cuenta A1 fue actualizada')
        })
    
        it('Delete a Client Account', () => {
            cy.visit('/panel/cuentas')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('#tbl-accounts tbody tr:last td:first')
                .should('have.text', inputData)
            //Clic on delete
            cy.get('#tbl-accounts tbody tr:last .btn-group .btn')
                .click()
            cy.get('.btn-group.show .btnAccountDelete').contains('Eliminar')    
                .click()
            cy.contains('Si')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.alert')
                .should('contain.text', inputData + ' fue eliminada')
        })
    })
})
