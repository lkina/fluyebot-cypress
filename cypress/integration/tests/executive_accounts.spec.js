import {admin} from "../../fixtures/usersData"
import {date} from "../../fixtures/date"

context('Executive accounts', () => {

    before(() => {
        cy.clearCookies()
        cy.intercept('GET', '**/user/auth').as('getAuth')
        cy.intercept('GET', '**/panel/campanias*').as('getCampanias')
        cy.intercept('GET', '**/panel/conversations*').as('getConversations')
        cy.visit('/')
        cy.login(admin)
        cy.wait(['@getAuth', '@getCampanias', '@getConversations'])
    })

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('fluyebot_session')
        Cypress.Cookies.preserveOnce('XSRF-TOKEN')
    })

    describe('Paths and Alerts', () => {

        it('Go to New Executive', () => {
            //Go to Ejecutivos
            cy.get('a.nav-link')
                .contains('Ejecutivos de ventas')
                .click()
            cy.url().should('include', '/panel/ejecutivos')
            //Go to Crear cuenta
            cy.contains('Crea un ejecutivo de ventas')
                .click()
            cy.url().should('include', '/panel/ejecutivo/crear')
        })

        it('Go to Edit a Executive', () => {
            cy.visit('/panel/ejecutivos')
            cy.wait(1000)
            cy.get('#ejecutivos tbody tr:first td').eq(2)
                .should('contain.text', 'Quantico')
            cy.get('#ejecutivos tbody tr:first .btn-secondary')
                .click()
            cy.url()
                .should('include', 'panel/ejecutivo/editar/8')
        })

        it('Go back to Executives List', () => {
            cy.visit('panel/ejecutivo/editar/8')
            cy.contains('Lista de ejecutivos de ventas')
                .click()
            cy.url().should('include', '/panel/ejecutivos')
        })
    
        it('Check Alert messages in New Executive Form', () => {
            //On Crear Cuenta
            cy.visit('/panel/ejecutivo/crear')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Enter data
            cy.get('#given_name')
                .type('Ejecutivo')
            cy.get('#fathers_last_name')
                .type('Maestro')
            cy.get('#email')
                .type('ejecutivo@empresa')
            cy.get('#password')
                .type('empresa2021')
            cy.get('#password-confirm')
                .type('empresa20')
            cy.contains('Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.help-block.text-danger')
                .should('contain.text', 'El campo correo electrónico debe ser una dirección de correo válida.')
                .and('contain.text', 'El campo teléfono debe contener entre 7 y 15 dígitos.')
                .and('contain.text', 'El campo confirmación de contraseña no coincide.')
        })
    })

    describe('Modify Executive Accounts', () => {

        const inputData = 'Ejecutivo ' + date

        it('Create a New Executive Account', () => {
            cy.visit('/panel/ejecutivo/crear')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Enter data
            cy.get('#given_name')
                .type(inputData)
            cy.get('#fathers_last_name')
                .type('Quantico')
            cy.get('#email')
                .type('ejecutivo' + date + '@empresa.com')
            cy.get('#phone_number')
                .type('999999999')
            cy.get('#password')
                .type('empresa2021')
            cy.get('#password-confirm')
                .type('empresa2021')
            cy.contains('Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.alert')
                .should('contain.text', inputData + ' Quantico fue registrado/a')
            cy.url().should('include', 'panel/ejecutivos')
        })
    
        it('Edit an Executive Account', () => {
            //Edit Ejecutivo Maestro
            cy.visit('panel/ejecutivo/editar/11')
            //Enter name and signature
            cy.get('#given_name').clear()
                .type('Ejecutivo')
            cy.get('#fathers_last_name').clear()
                .type('Maestro')
            cy.get('#email').clear()
                .type('ejecutivo@empresa.com')
            cy.get('#phone_number').clear()
                .type('7390777')
            cy.get('#password').clear()
                .type('empresa2021')
            cy.get('#password-confirm').clear()
                .type('empresa2021')
            cy.contains('Guardar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.alert')
                .should('contain.text', 'Ejecutivo Maestro fue actualizado/a')
        })
    
        it('Deactivate/Activate an Executive Account', () => {
            cy.visit('/panel/ejecutivos')
            //Deactivate bot
            cy.get('#ejecutivos tbody tr:last .btn-danger')
                .click()
            cy.get('[role=dialog]').contains('Aceptar')
                .click()
            cy.wait(1000)
            cy.get('#ejecutivos tbody tr:last .btn-primary i')
                .should('have.class', 'fa-check')
            //Activate bot
            cy.get('#ejecutivos tbody tr:last .btn-primary')
                .click()
            cy.get('[role=dialog]').contains('Aceptar')
                .click()
            cy.wait(1000)
            cy.get('#ejecutivos tbody tr:last .btn-danger i')
                    .should('have.class', 'fa-remove')
        })
    })
})
