import {admin} from "../../fixtures/usersData"
import {date} from "../../fixtures/date"

context('Flows', () => {
    before(() => {
        cy.clearCookies()
        cy.intercept('GET', '**/user/auth').as('getAuth')
        cy.intercept('GET', '**/panel/campanias*').as('getCampanias')
        cy.intercept('GET', '**/panel/conversations*').as('getConversations')
        cy.visit('/')
        cy.login(admin)
        cy.wait(['@getAuth', '@getCampanias', '@getConversations'])
    })

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('fluyebot_session')
        Cypress.Cookies.preserveOnce('XSRF-TOKEN')
    })

    describe('Paths and Alerts', () => {

        it('Go to New Flow', () => {
            //Go to Flujos de secuencia
            cy.get('a.nav-link')
                .contains('Flujos de secuencia')
                .click()
            cy.wait(1000)
            cy.url().should('include', '/panel/flows')
            //Go to Nuevo flujo
            cy.contains('Crear nuevo flujo')
                .click()
            cy.get('.modal').should('be.visible')
        })

        it('Go to Edit a Flow', () => {
            //Edit flow 22, "Flujo de prueba"
            cy.visit('/panel/flows')
            cy.wait(1000)
            cy.get('#flow_filter')
                .type('Flujo de prueba')
            cy.get('#flow tbody .btn-secondary')
                .click()
            cy.wait(1000)
            cy.url()
                .should('include', 'panel/flow/22')
        })

        it('Go back to Flows List', () => {
            cy.visit('panel/flow/22')
            cy.contains('Lista de flujos')
                .click()
            cy.url().should('include', '/panel/flows')
        })
    })

    describe('Modify Flows', () => {

        const inputData = 'Flujo ' + date

        it('Create a New Flow', () => {
            cy.visit('/panel/flows')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            //Enter data
            cy.get('.modal #flow-name')
                .type(inputData, {force: true})
            cy.get('#crearflow')
                .click({force: true})
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.url()
                .should('include', 'panel/flow')
            cy.get('.card #flow_name')
                .should('have.value', inputData)
        })
    
        it('Edit an existing Flow', () => {
            //Edit flow 22, "Flujo de prueba"
            cy.visit('panel/flow/22')
            //Enter name and description
            cy.get('#flow_name').clear()
                .type('Flujo de prueba')
            cy.get('#flow_description').clear()
                .type('Flujo de prueba para editar')
            cy.contains('Actualizar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.swal2-confirm').contains('Actualizar')
                .click()
            cy.get('#swal2-content')
                .should('have.text', 'Flujo de secuencia actualizada correctamente')
        })

        it('Duplicate an existing Flow', () => {
            cy.visit('/panel/flows')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('#flow_filter')
                .type(inputData)
            cy.get('#flow tbody tr:last .btn-primary').contains('Duplicar')
                .click()
            cy.get('.swal2-confirm').contains('Enviar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('#flow_filter')
                .type(inputData + ' (copia)')
            cy.get('#flow tbody tr')
                .should('have.length.gte', 1)
        })
    })

    describe('Modify Sequences', () => {

        /*it.skip('Add a New Sequence', () => {
            //Not able to drag and drop canvas element

            cy.visit('/panel/flow/22')
            cy.wait(1000)

            //With native trigger events
            cy.get("#myPaletteDiv>canvas").focus()
                .trigger("mousedown", { which: 1 }, {force:true})
            cy.get("#myDiagramDiv>canvas").focus()
                .trigger("mousemove", {force:true})
                .trigger("mouseup", {force:true})

            //With dragstart and dragend
            const dataTransfer = new DataTransfer()
            cy.get("#myPaletteDiv>canvas")
                .trigger("dragstart", { dataTransfer }, {force:true})
            cy.get("#myDiagramDiv>canvas").focus()
                .trigger("drop", { dataTransfer }, {force:true})            
            cy.get("#myPaletteDiv>canvas")
                .trigger("dragend", {force:true})


            //With drag and drop plugin
            cy.get('#myPaletteDiv>canvas').drag('#myDiagramDiv>canvas')

            //With coordinates
            cy.get('#myDiagramDiv>canvas').then(($el) => {
                const x1 = $el[0].getBoundingClientRect().left;
                const x2 = $el[0].getBoundingClientRect().width;
                const xc = x1 + x2 / 2;
                const y1 = $el[0].getBoundingClientRect().top;
                const y2 = $el[0].getBoundingClientRect().height;
                const yc = y1 + y2 / 2;
                cy.get('#myPaletteDiv>canvas')
                    .trigger('mousedown')
                    .trigger('mousemove', { clientX: xc, clientY: yc })
                    .trigger('mouseup')
            })
            // ==== Generated with Cypress Studio ====
            cy.get('[style="heigh:100%; width:100%; white-space:nowrap;"]').click();
            cy.get('#myDiagramDiv > canvas').click();
            // ==== End Cypress Studio ====
        })*/

        it('Edit a existing Sequence', () => {
            
            const newMessage = date + ' Hola $_LEAD_NOMBRE,{enter}Le escribo para informarle que tiene una tarjeta de crédito disponible.{enter}Saludos cordiales.'
            
            cy.visit('/panel/flow/22')
            cy.wait(1000)
            cy.get('#myDiagramDiv > canvas').click()

            //Edit Secuencia 2 mensaje 1
            cy.get(':nth-child(1) > .card-body > #message > :nth-child(3) > .note-editor > .note-editing-area > .note-editable')
                .clear()
                .type(newMessage)
            cy.contains('Guardar')
                .click()
            cy.get('#swal2-content')
                .should('contain.text', 'Secuencia creada')
            cy.wait(1000)

            //Validate Secuencia 1
            cy.request({
                method: 'GET',
                url: '/panel/message_sequence/?flow_node_id=65&_token=uVOoEYf2wLYTKx0pi7EjhGYQp68nK7B9qwKlLiJQ'
            }).then((response) => {
                expect(response.body).to.include('información sobre la tarjeta de crédito que tiene aprobada con nosotros?.')
            })

            //Validate Negative response
            cy.request({
                method: 'GET',
                url: '/panel/message_sequence/?flow_node_id=70&_token=uVOoEYf2wLYTKx0pi7EjhGYQp68nK7B9qwKlLiJQ'
            }).then((response) => {
                expect(response.body).to.include('type="checkbox" checked')
                    .and.to.include("<option selected value='negative'>Negativo</option>")
            })
        })
    })
})
