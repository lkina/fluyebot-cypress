import {superadmin} from "../../fixtures/usersData"

describe('Leads', () => {

    before(() => {
        cy.clearCookies()
        cy.intercept('GET', '**/user/auth').as('getAuth')
        cy.intercept('GET', '**/panel/conversations/accounts').as('getAccounts')
        cy.intercept('GET', '**/panel/conversations*').as('getConversations')
        cy.visit('/')
        cy.login(superadmin)
        cy.wait(['@getAuth', '@getConversations', '@getAccounts'])
    })

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('fluyebot_session')
        Cypress.Cookies.preserveOnce('XSRF-TOKEN')
    })

    it('Check filters', () => {
        //Go to superadmin leads, from 2021-01-01 to 2021-06-25
        cy.visit('/panel/conversations?queryString=&valuation=POSITIVE&itemsPerPage=20&page=1&dateStart=2021-01-01&dateFinish=2021-06-25')
        cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
        //Check search bar
        cy.get('[placeholder="Texto a buscar"]')
            .type('sergio{enter}')
        cy.get('.tab-pane .timeline-item')
            .should('have.length', 1)
        cy.get('[placeholder="Texto a buscar"]')
            .clear().type('{enter}')
        //Cuenta:capsule, campañas:campaña5000 y campaña999, estado:por contactar
        //Positive
        cy.visit('/panel/conversations?queryString=&valuation=POSITIVE&itemsPerPage=20&page=1&state%5B%5D=NONE&channel%5B%5D=VISIT&channel%5B%5D=CALL&channel%5B%5D=INFO&channel%5B%5D=OTHERS&accountId%5B%5D=1&campaignId%5B%5D=21&campaignId%5B%5D=20&dateStart=2021-01-01&dateFinish=2021-06-25')
        cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
        cy.get('.tab-pane .timeline-item')
            .should('have.length', 2)
        //Negative
        cy.visit('/panel/conversations?queryString=&valuation=NEGATIVE&itemsPerPage=20&page=1&state%5B%5D=NONE&channel%5B%5D=VISIT&channel%5B%5D=CALL&channel%5B%5D=INFO&channel%5B%5D=OTHERS&accountId%5B%5D=1&campaignId%5B%5D=21&campaignId%5B%5D=20&dateStart=2021-01-01&dateFinish=2021-06-25')
        cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
        cy.contains('No hay resultados')
            .should('be.visible')
        //In progress
        cy.visit('/panel/conversations?queryString=&valuation=IN_PROCESS&itemsPerPage=20&page=1&state%5B%5D=NONE&channel%5B%5D=VISIT&channel%5B%5D=CALL&channel%5B%5D=INFO&channel%5B%5D=OTHERS&accountId%5B%5D=1&campaignId%5B%5D=21&campaignId%5B%5D=20&dateStart=2021-01-01&dateFinish=2021-06-25')
        cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
        cy.contains('No hay resultados')
            .should('be.visible')
    })

    it('Check a Lead', () => {
        cy.visit('http://stagebot.inventarteam.com/panel/conversations?queryString=&valuation=POSITIVE&itemsPerPage=20&page=1&state%5B%5D=NONE&channel%5B%5D=VISIT&channel%5B%5D=CALL&channel%5B%5D=INFO&channel%5B%5D=OTHERS&accountId%5B%5D=1&campaignId%5B%5D=21&campaignId%5B%5D=20&dateStart=2021-01-01&dateFinish=2021-06-25')
        cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
        cy.get('.timeline-item:first button')
            .contains('Ver conversación')
            .click()
        cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
        cy.get('.header')
            .should('contain.text', 'Información del lead')
        cy.get('.body')
            .should('contain.text', 'renzo@fluye.com')
            .and('contain.text', 'campaña 5000')
            .and('contain.text', 'Positivo')
        cy.get('.clearfix .message.float-left')
            .should('exist')
    })
})
