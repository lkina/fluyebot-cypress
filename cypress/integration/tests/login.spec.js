// <reference types="cypress" />
// @ts-check
import { superadmin, admin, executive } from "../../fixtures/usersData"

describe('Login', () => {
    beforeEach(() => {
        cy.intercept('GET', '**/user/auth').as('getAuth')
        cy.intercept('GET', '**/panel/conversations/accounts').as('getAccounts')
        cy.intercept('GET', '**/panel/campanias*').as('getCampanias')
        cy.intercept('GET', '**/panel/conversations*').as('getConversations')
        cy.visit('/')
    })
    
    it('Load homepage', () => {
        cy.contains('Login')
    })
    
    it('Get empty fields messages', () => {
        cy.get('.btn').click()
        cy.contains('El campo correo electrónico es obligatorio.').should('exist')
        cy.contains('El campo contraseña es obligatorio.').should('exist')
    })

    it('Check the checkbox', () => {
        cy.get('[type="checkbox"]')
            .check({ force: true })
            .should('be.checked')
            .uncheck({ force: true })
            .should('not.be.checked')
    })    

    it('Check the Forgot Password link', () => {
        cy.contains('¿Olvidaste tu contraseña?').click()
        cy.url().should('include', '/reset')
    })

    it('Input invalid credentials', () => {
        cy.get('#email')
            .type('noname@fluye.com')  
        cy.get('#password')
            .type('nopassword')
        cy.get('.btn').click()
        cy.contains('Estas credenciales no coinciden con nuestros registros.')
            .should('exist')
    })

    it('Input valid SuperAdmin credentials', () => {
        cy.get('#email')
            .type(superadmin.email)
            .should('have.value', superadmin.email)   
        cy.get('#password')
            .type(superadmin.password)
            .should('have.value', superadmin.password)
        cy.get('.btn').click()
        cy.wait(['@getAuth', '@getAccounts', '@getConversations'])
        cy.url().should('include', '/conversations?')
        cy.get('.navbar-right #navbar-menu .nav.navbar-nav .mt-2 a small')
            .should('contain', superadmin.email)
        //Logout
        cy.get('button > .fa').click()
        cy.url().should('include', '/login')
    })

    it('Input valid Admin credentials', () => {
        cy.get('#email')
            .type(admin.email)
            .should('have.value', admin.email)   
        cy.get('#password')
            .type(admin.password)
            .should('have.value', admin.password)
        cy.get('.btn').click()
        cy.wait(['@getAuth', '@getCampanias', '@getConversations'])     
        cy.url().should('include', '/conversations?')
        cy.get('.navbar-right #navbar-menu .nav.navbar-nav .mt-2 a small')
            .should('contain', admin.email)
        //Logout
        cy.get('button > .fa').click()
        cy.url().should('include', '/login')
    })

    it('Input valid Executive credentials', () => {
        cy.get('#email')
            .type(executive.email)
            .should('have.value', executive.email)   
        cy.get('#password')
            .type(executive.password)
            .should('have.value', executive.password)
        cy.get('.btn').click()
        cy.wait(['@getAuth', '@getCampanias', '@getConversations'])
        cy.url().should('include', '/conversations?')
        cy.get('.navbar-right #navbar-menu .nav.navbar-nav .mt-2 a small')
            .should('contain', executive.email)
        //Logout
        cy.get('button > .fa').click()
        cy.url().should('include', '/login')
    })
})

