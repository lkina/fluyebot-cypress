import {superadmin, admin, executive} from "../../fixtures/usersData"

context('User level permissions', () => {
    beforeEach(() => {
        cy.intercept('GET', '**/user/auth').as('getAuth')
        cy.intercept('GET', '**/panel/conversations/accounts').as('getAccounts')
        cy.intercept('GET', '**/panel/campanias*').as('getCampanias')
        cy.intercept('GET', '**/panel/conversations*').as('getConversations')
        cy.visit('/')
    })

    describe('Header', () => {
        const userInfo = '.navbar-right #navbar-menu .nav.navbar-nav .mt-2 a small'

        it('Check Superadmin display', () => {
            cy.login(superadmin)
            cy.wait(['@getAuth', '@getAccounts', '@getConversations'])
            cy.get(userInfo)
                .should('include.text', superadmin.email)
            cy.get('#navbarsExample05 .navbar-nav.mr-auto:first li')
                .should('have.length', 6)
            cy.get('#navbarsExample05 .navbar-nav.mr-auto').eq(2)
                .should('include.text', 'Listado de bases de usuarios')
        })

        it('Check Admin display', () => {
            cy.login(admin)
            cy.wait(['@getAuth', '@getCampanias', '@getConversations']) 
            cy.get(userInfo)
                .should('include.text', admin.email)
            cy.get('#navbarsExample05 .navbar-nav.mr-auto:first li')
                .should('have.length', 6)
            cy.get('#navbarsExample05 .navbar-nav.mr-auto').eq(2)
                .should('include.text', 'Cargar bases de usuarios')
        })

        it('Check Executive display', () => {
            cy.login(executive)
            cy.wait(['@getAuth', '@getCampanias', '@getConversations']) 
            cy.get(userInfo)
                .should('include.text', executive.email)
            cy.get('#navbarsExample05 .navbar-nav.mr-auto:first li')
                .should('have.length', 1)
        })
    })

    describe('Leads', () => {
        
        it('Check Superadmin display', () => {
            cy.login(superadmin)
            cy.wait(['@getAuth', '@getAccounts', '@getConversations']) 
            cy.get('a.nav-link')
                .contains('Leads')
                .click()    
            cy.wait(['@getAuth', '@getAccounts', '@getConversations'])        
            cy.url().should('include', 'panel/conversations')
            cy.get('.body > .nav li')
                .should('have.length', 3)
        })

        it('Check Admin display', () => {
            cy.login(admin)
            cy.wait(['@getAuth', '@getCampanias', '@getConversations'])
            cy.get('a.nav-link')
                .contains('Leads')
                .click()
            cy.wait(['@getAuth', '@getCampanias', '@getConversations'])
            cy.url().should('include', 'panel/conversations')
            cy.get('.body > .nav li')
                .should('have.length', 3)
        })

        it('Check Executive display', () => {
            cy.login(executive)
            cy.wait(['@getAuth', '@getCampanias', '@getConversations'])
            cy.get('a.nav-link')
                .contains('Leads')
                .click()
            cy.wait(['@getAuth', '@getCampanias', '@getConversations'])
            cy.url().should('include', 'panel/conversations')
            cy.get('.body > .nav li')
                .should('have.length', 0)
        })
    })
})