import {admin} from "../../fixtures/usersData"

context('Prospects', () => {
    
    before(() => {
        cy.clearCookies()
        cy.intercept('GET', '**/user/auth').as('getAuth')
        cy.intercept('GET', '**/panel/campanias*').as('getCampanias')
        cy.intercept('GET', '**/panel/conversations*').as('getConversations')
        cy.intercept('**panel/destinatarios/crear-lista').as('createList')
        cy.visit('/')
        cy.login(admin)
        cy.wait(['@getAuth', '@getCampanias', '@getConversations'])
    })

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('fluyebot_session')
        Cypress.Cookies.preserveOnce('XSRF-TOKEN')
    })

    describe('Paths and alerts', () => {
        it('Check routes', () => {
            //Go to prospects
            cy.get('a.nav-link')
                .contains('Prospectos')
                .click()
            cy.url().should('include', '/destinatarios/listas')
            //Go to new list
            cy.get('.header > .btn')
                .click()
            cy.wait('@createList')
            cy.url().should('include', '/destinatarios/crear-lista')
            //Go back to all lists
            cy.get('.breadcrumb-item > a')
            cy.get('.breadcrumb-item').contains('Listas')
                .click()
            cy.url().should('include', '/destinatarios/listas')
        })

        it('Check alert messages on New List form', () => {
            cy.visit('/panel/destinatarios/crear-lista')
            cy.get('#cargar').contains('Cargar')
                .click()
            cy.get('.alert').find('[data-style="style8"]')
                .should('include.text', 'El campo nombre es obligatorio.')
                .and('include.text', 'El campo descripción es obligatorio.')
            cy.reload()
            cy.wait(1000)
            //Enter data
            cy.get('#name')
                .type('Lista de prueba sin csv')
            cy.get('#description')
                .type('Esta es una lista de prueba para los tests en Cypress')
            cy.get('#cargar').contains('Cargar')
                .click()
            cy.get('.alert').should('include.text', 'Debe incluir un archivo csv')    
        })
    })

    describe('Search bars', () => {

        it('Search and go to edit a List', () => {
            cy.visit('/panel/destinatarios/listas')        
            //Search a List
            cy.get('#addresse_filter').find('[aria-controls="addresse"]')
                .type('list ID 107')
            cy.get('[aria-describedby="addresse_info"] tbody tr')
                .should('have.length', 1)
            //Edit a list        
            cy.get('.btn-secondary').click()
            cy.url().should('include', '/editar-lista/107')
        })

        it('Search a prospect', () => {
            cy.visit('/panel/destinatarios/editar-lista/107')
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('#destinatarios_filter').find('input.form-control').type('lkina')
            cy.get('#destinatarios tbody tr')
                .should('have.length', 2)
        })
    })

    describe('Modify lists', () => {

        it('Create a new List', () => {
            cy.visit('/panel/destinatarios/crear-lista')
            //Enter data
            cy.get('#name')
                .type('Create/Delete Test List')
            cy.get('#description')
                .type('Esta es una lista de prueba para los tests en Cypress')
            cy.get('#csv_file')
                .attachFile('../fixtures/BD_usuariosdeprueba.csv')
            cy.get('#cargar').contains('Cargar')
                .click()
            cy.wait(1000)
            cy.get('.alert').should('include.text', 'Se importaron 10 leads')
            cy.get('#listEmailsBlack').should('exist')
            cy.get('#destinatarios_filter .form-control')
                .type('lkina')
            cy.get('#destinatarios tbody tr')
                .should('have.length', '2')
        })
       
        it('Add new addressees', () => {
            cy.visit('/panel/destinatarios/editar-lista/107')
            cy.get('#name').clear()
                .type('Updated Test List ID 107')
            cy.get('#description').clear()
                .type('Lista de prueba para editar')
            cy.get('#csv_file')
              .attachFile('../fixtures/BD_usuariosdeprueba_con1.csv')
            cy.get('#cargar').contains('Actualizar')
               .click()
            cy.wait(1000)
            cy.get('.alert').should('include.text', 'Se importaron 10 leads')
            cy.get('#listEmailsBlack').should('exist')
            cy.get('.totalLegends .item-legend:last .badge')
                .should('have.text', '20')
        })
        
        it('Replace addressees', () => {
            cy.visit('/panel/destinatarios/editar-lista/107')
            cy.get('#name').clear()
                .type('Replaced Test List ID 107')
            cy.get('#description').clear()
                .type('Esta es una lista de prueba para editar')
            cy.get('#csv_file')
                .attachFile('../fixtures/BD_usuariosdeprueba.csv')
            cy.get('[type="checkbox"]')
                .check({ force: true })
                .should('be.checked')
            cy.get('#cargar').contains('Actualizar')
                .click()
            cy.wait(1000)
            cy.get('.alert').should('include.text', 'Se importaron 10 leads')
            cy.get('#listEmailsBlack').should('exist')
            cy.get('.totalLegends .item-legend:last .badge')
                .should('have.text', '10')
        })
    
        it('Delete lists', () => {
            cy.visit('/panel/destinatarios/listas')
            //Delete list created in "Create a new List"
            cy.get('#addresse_filter').find('[aria-controls="addresse"]')
                .type('"Create/Delete Test List"')
            cy.get('table#addresse tbody tr:first')
                .find('[type=submit]')
                .click() 
            //Confirmation pop-up
            cy.get('[role=dialog]').contains('Aceptar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.alert')
                .should('contain.text', 'Prospecto eliminado correctamente')
            cy.get('#addresse_filter').find('[aria-controls="addresse"]')
                .type('"Create/Delete Test List"')
            cy.get('table#addresse tbody tr td')
                .should('contain.text', 'No se encontraron resultados')
            //Delete list created in "Check alert messages on New List form"
            cy.get('#addresse_filter').find('[aria-controls="addresse"]').clear()
                .type('"Lista de prueba sin csv"')
            cy.get('[aria-describedby="addresse_info"] tbody tr:first')
                .find('[type=submit]')
                .click()
            //Confirmation pop-up
            cy.get('[role=dialog]').contains('Aceptar')
                .click()
            cy.wait(1000) //Wait for /plug-ins/1.10.16/i18n/Spanish.json
            cy.get('.alert')
                .should('contain.text', 'Prospecto eliminado correctamente')
            cy.get('table#addresse tbody tr').should('have.length', '2')
        })
    })
})    